import { ZIP_CHANGED,FETCH_SUCCESS,LOADING} from '../reducers/type';
export const zipChanged=(text)=>{
  return {
    type: ZIP_CHANGED,
    payload: text
  };
};

export const weatherFetch=(data)=>{
  return {
    type: FETCH_SUCCESS,
    payload: data
  };
};

export const InitiateLoading=(data)=>{
  console.log('InitiateLoading');
  return {
    type: LOADING,
    payload: data
  };
};
