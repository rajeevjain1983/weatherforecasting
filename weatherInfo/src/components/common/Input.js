import React from 'react';
import { TextInput, Text, View } from 'react-native';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
return (
          <View style={styles.containerStyle}>
            <Text style={styles.textStyle}> {label}</Text>
            <TextInput
            secureTextEntry={secureTextEntry}
            placeholder={placeholder}
            autoCorrect={false}
            style={styles.inputStyle}
            value={value}
            onChangeText={onChangeText}

            />
          </View>
  );
  };

  const styles = {

    inputStyle: {
      color: '#000',
      paddingRight: 5,
      paddingLeft: 5,
      fontSize: 18,
      flex: 2
    },
    textStyle: {
      fontSize: 18,
      paddingLeft: 10,
      flex: 1
    },
    containerStyle: {
      height: 40,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',

    }
  };

export { Input };
