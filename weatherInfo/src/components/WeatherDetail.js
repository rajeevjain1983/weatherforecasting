import React, { Component } from 'react';
import {View, Text} from 'react-native';
import axios from 'axios';
import { connect } from 'react-redux';
import {zipChanged,weatherFetch,InitiateLoading} from '../actions';
import { Card,CardSection, Input, Button,Spinner } from './common';
import WeatherGrid from './WeatherGrid'



class WeatherDetail extends Component {
onEmailChanged(text){
  //alert(text);
this.props.zipChanged(text);
}
ButtonClicked()
{
  console.log(this.props.loading);
  this.props.InitiateLoading(true);
  console.log(this.props.loading);
  let weatherUrl=`https://samples.openweathermap.org/data/2.5/forecast?zip=${this.props.zipCode}&appid=b6907d289e10d714a6e88b30761fae22`;
  axios.get(weatherUrl)
  .then(response =>this.fetchSuccess(response.data))
  .catch(error=>this.failSuccess(error));
}

failSuccess(error)
{
  console.log(error);
  //this.props.InitiateLoading(false);
  console.log(this.props.loading);
}
fetchSuccess(data)
{
  console.log(this.props.loading);
  this.props.weatherFetch(data);
  this.props.InitiateLoading(false);

  console.log('fetchSuccess');
  console.log(data);
  console.log(this.props.loading);
}
renderGrid()
{
  if(this.props.weatherInfo.city && this.props.loading==false)
  {
    return <WeatherGrid weatherInfo={this.props.weatherInfo} />
  }
}
renderButton()
{
  if(this.props.loading==false)
  {
      return (
        <Button onPress={this.ButtonClicked.bind(this)}>
          Get Weather Info
        </Button>
      );
  }

  return (
    <Spinner />
  );

}


  render(){
    return(
      <View style={Styles.containerStyle}>
      <Card>
        <CardSection>
            <Input
            label='Zip code :'
            placeholder='122001'
            value={this.props.email}
            onChangeText={this.onEmailChanged.bind(this)}
            />
        </CardSection>
        <CardSection>

        {this.renderButton()}

        </CardSection>

      </Card>
      <View style={Styles.gridStyle}>
      {this.renderGrid()}
      </View>
      </View>
    );
  }
}
const Styles ={
  containerStyle: {
    marginBottom:50,
    flex:1
  },
  gridStyle:{
    flex:1,
    marginTop:10,
  }
};
const mapStateToProps=state=>{
  //console.log(state);
  return {
    zipCode:state.UserInput.zip,
    weatherInfo:state.WeatherInfo,
    loading:state.UserInput.loading
  };
}
export default connect(mapStateToProps,{zipChanged,weatherFetch,InitiateLoading})(WeatherDetail);
