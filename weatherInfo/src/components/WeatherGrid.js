import React, { Component } from 'react';
import {Text, View, FlatList } from 'react-native';
import { Card,CardSection} from './common';
import ListItem from './ListItem';




class WeatherGrid extends Component {
  renderItem(item) {
  return <ListItem containerStyle={styles.containerStyle} textStyle={styles.textStyle} rowItem={item}
  />
}

renderHeader() {

  return (
      <View style={styles.headerContainer}>

    <Text style={styles.textStyleHeader}>Date Time</Text>
    <Text style={styles.textStyleHeader}>Min Temp.</Text>
    <Text style={styles.textStyleHeader}>Max Temp.</Text>
    <Text style={styles.textStyleHeader}>Humidity</Text>
    <Text style={styles.textStyleHeader}> </Text>
    </View>
  );
}


  render(){
    console.log('Grid');
    const {city,list} =this.props.weatherInfo;
  if(city)
  {
      return(
        <View style={styles.viewStyle}>
            <Card>
              <CardSection>
                  <Text style={{fontSize:15,fontWeight:'bold'}} >City : </Text><Text>{city.name}</Text>
                  <Text style={{fontSize:15,fontWeight:'bold'}}>  Country : </Text><Text >{city.country}</Text>
              </CardSection>
            </Card >
                <FlatList
                  style={styles.listStyle}
                    data={list}
                    renderItem={this.renderItem}
                    ListHeaderComponent={this.renderHeader}
                                  keyExtractor={item => item.dt_txt}
                                  />

            </View>
      );
  }
  return <Card />
  }
}
const styles = {
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex:1
  },
  listStyle:{
    margin: 10,
  },
  textStyle: {
    fontSize:13,
    width:70,
    textAlign:'center',
    alignSelf:'center'
  },
  textStyleHeader: {
    fontSize:15,
    width:70,
    textAlign:'center',
    alignSelf:'center',
    fontWeight:'bold',
    color: '#007aff',
  },
  headerContainer:{
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor:'#ddd'
  },
  containerStyle:{
    justifyContent: 'center',
    flexDirection: 'row',
  },

    
};
export default WeatherGrid
