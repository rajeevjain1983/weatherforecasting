import React, { Component } from 'react';
import { Text, View,Image} from 'react-native';
import { Card,CardSection} from './common';


class ListItem extends Component {
  render(){
    const {item}=this.props.rowItem;
if(item)
{
  const imagURL=`http://openweathermap.org/img/w/${item.weather[0].icon}.png`
      return (

                <View style={this.props.containerStyle}>
                  <Text style={this.props.textStyle}> {item.dt_txt}</Text>
                  <Text style={this.props.textStyle}> {item.main.temp_min}</Text>
                  <Text style={this.props.textStyle}> {item.main.temp_max}</Text>
                  <Text style={this.props.textStyle}> {item.main.humidity}</Text>
                  <Image
                         style={{width: 50, height: 20,marginTop:15}}
                         source={{uri: imagURL}}
                       />
                       </View>
      );
  }
  }
}

export default ListItem;
