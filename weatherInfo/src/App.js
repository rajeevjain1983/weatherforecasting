import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore }  from 'redux';
import reducers from './reducers';
import { Header } from './components/common';

import WeatherDetail from './components/WeatherDetail';

class App extends Component {
  render(){
    return(
      <Provider store={createStore(reducers)}>
      <View style={{flex:1,marginTop:40}}>
        <Header headerText="Weather Forecasting" />
        <WeatherDetail />
      </View>
      </Provider>
    );
  }
}
export default App;
