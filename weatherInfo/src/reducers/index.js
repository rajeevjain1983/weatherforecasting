import { combineReducers } from 'redux';
import zipReducer from './zipReducer';
import weatherReducer from './weatherReducer';

export default combineReducers({
  UserInput:zipReducer,
  WeatherInfo:weatherReducer
});
