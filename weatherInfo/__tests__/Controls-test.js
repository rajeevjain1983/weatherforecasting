import React from 'react';
import { Header, Button, Card,CardSection, Input, Spinner } from '../src/components/common';
import WeatherDetail from '../src/components/WeatherDetail';
import renderer from 'react-test-renderer';



test('Header render successfully',()=>{
  const ctrl=renderer.create(<Header headerText="ABCDe" />).toJSON();
  expect(ctrl).toMatchSnapshot();
});

test('Button render successfully',()=>{
  const ctrl=renderer.create(<Button  />).toJSON();
  expect(ctrl).toMatchSnapshot();
});

test('Card render successfully',()=>{
  const ctrl=renderer.create(<Card  />).toJSON();
  expect(ctrl).toMatchSnapshot();
});

test('Card Section render successfully',()=>{
  const ctrl=renderer.create(<CardSection  />).toJSON();
  expect(ctrl).toMatchSnapshot();
});

test('Input Control render successfully',()=>{
  const ctrl=renderer.create(<Input  />).toJSON();
  expect(ctrl).toMatchSnapshot();
});

test('Spinner Control render successfully',()=>{
  const ctrl=renderer.create(<Spinner  />).toJSON();
  expect(ctrl).toMatchSnapshot();
});
